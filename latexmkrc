$pdf_previewer = "zathura %O %S";
$pdf_update_method = 1;
$clean_ext = 'soc rel paux lox pdfsync acn acr alg aux bbl bcf blg brf fdb_latexmk glg glo gls idx tdo ilg ind ist lof log lot out run.xml toc dvi snm synctex.gz nav';
$pdflatex = 'xelatex %O -interaction=nonstopmode -synctex=1 %S';
$pdf_mode = 1;
